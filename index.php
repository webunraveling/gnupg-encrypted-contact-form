<?php

require_once( 'inc/header.php' );

$f = new Encrypted_Email_Form();

// checks for GPG and dies if not able to use it
// also creates GPG object for use elsewhere
$f->gpg_check( $gpgenv, $smtp_settings['recipient'] );

// print the form or handle form input
if ( empty( $_POST ) ){

    $f->print_form( $form_settings );

} elseif ( ! empty( $_POST ) ) {

    $f->send_email( $_POST, $smtp_settings, $gpgenv );

} else {

    return 'Something went wrong.';

}

require_once( 'inc/footer.php' );
