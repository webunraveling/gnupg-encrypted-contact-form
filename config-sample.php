<?php
/*
 * All settings and form customization are done here
 *
 * You must rename this file to config.php for it to take effect
 *
 */


// optional security key to be added to the URL to minimize chances
// of bots or unwanted people accessing your form
// example: yoursite.com/form/?key=a2b3n4m5
//define( 'KEY', 'changeme' ); // used in $_GET so random people can access this page

// note: you need to import your key with --import pubkey.asc
$gpgenv = '/home/username/.gnupg'; // path to public key directory

$form_settings = array(
    'host' => 'yoursite.com', // the domain of 
    'title' => 'Contact Me',
    'message' => 'Please fill out the form below. All information will be sent via an encrypted message.', // a message to appear before the form
    'fields' => array(
        // 'Field Name' => '', // if only fieldname is provided the input will be a text box 
        'Email' => array ( 'type' => 'email' ),
        'Name' => '',
        'Last Name' => '',
        'Phone Number' => '',
        // 'Field Name' => array('Option1','Option2','Option3'), // if options are provided the will appear as radio buttons
        'Favorite Season' => array( 'Spring','Summer','Fall','Winter' ),
        'Email Address' => array ( 'type' => 'email', 'honeypot' => true ),
    ),
);

// settings for sending mail
$smtp_settings = array(
    'noteToSelf' => 'The response below is from some page on yoursite.com.', // a note to yourself that appears at the beginning of the email.
    'recipient' => 'you@yoursite.com', // recipient's email address
    'sender' => 'no-reply@yoursite.com', // senders email address or username
    'subject' => '', // subject of the email (Optional) Will default to "Encrypted Email Form Submitted at TIMESTAMP" 
    'password' => 'superSecurePassword', // senders password
    'host' => 'yoursite.com', // the address of your email server
    'port' => '587', // standard ports are usually 25, 465, 587
);

