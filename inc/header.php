<?php

require_once( 'config.php' );
include_once( 'inc/safety-check.php' ); // checks $_GET key if set in config
require_once( 'inc/class-encrypted-email-form.php' );

// get phpmailer ... be sure you have installed it using composer
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require( 'vendor/autoload.php' ); // Load Composer's autoloader

?>

<!doctype html>
<html>
    <head>
        <title><?php echo $form_settings['title']; ?></title>
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
        <link rel="stylesheet" type="text/css" href="style/style.css" />
    </head>
    <body>
        <div class="wrapper">
