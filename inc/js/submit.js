/* 
 * This is non-essential but will prevent accidental
 * double clicking of the submit button
 */

document.getElementById('submitBtn').addEventListener('click', function(){
    this.disabled = true;
    this.form.submit();
});
