<?php
/*
 * Build the form for user input.
 */

class Encrypted_Email_Form {

    /*
     * Print the form
     *
     * @param array $form
     */
    public function print_form( $form = array() ) {

        if ( ! isset( $form ) ) {
            throw new Exception( '<p>print_form requires an array as parameter.</p>' );
        }

        $host    = ( isset( $form['host'] ) ? $form['host'] : $_SERVER['SERVER_ADDR'] );
        $title   = $form['title'];
        $message = $form['message'];
        $fields  = $form['fields'];

        echo ( isset( $title ) ? '<h1 class="form-title">' . $title . '</h1>' : '' );
        echo ( isset( $message ) ? '<p class="form-message">' . $message . '</p>' : '' );

        $safety_key = ( defined( 'KEY' ) ? '?key=' . KEY : null );

        echo '<form name="encrypted-contact-form" id="encrypted-contact-form" method="post" action="' . basename( $_SERVER['PHP_SELF'] ) . ( null !== $safety_key ? $safety_key : '' ) . '">';

        foreach ( $fields as $field => $settings ) {

            $field_id    = strtolower( str_replace( ' ', '-', $field ) );
            $field_value = ( empty( $_GET[$field_id] ) ? '' : $_GET[$field_id] );

            echo sprintf( '<div class="parent-field%s"><label class="field-name" for="%s">%s</label><br />',
                ( empty( $settings['honeypot'] ) ? '' : ' ishrunkthekids' ),
                $field,
                $field
            );

            /**********************************************************
            /* decide on the type of input to go with the label above
            /*********************************************************/

            // text field
            if ( empty( $settings ) ) {

                printf( '<input type="text" id="%1$s" name="%2$s" value="%3$s" />',
                    $field_id,
                    $field,
                    $field_value
                );


            } elseif ( 'textarea' === $settings['type'] ) {


                printf( '<textarea id="%1$s" name="%2$s" rows="4">%3$s</textarea>',
                    $field_id,
                    $field,
                    $field_value
                );

            } elseif ( 'email' === $settings['type'] ) {


                printf( '<input type="email" id="%1$s" name="%2$s" value="%3$s" />',
                    $field_id,
                    $field,
                    $field_value
                );


            } elseif ( 'password' === $settings['type'] ) {


                printf( '<input type="password" id="%1$s" name="%2$s" />',
                    $field_id,
                    $field
                );


            } elseif ( 'radio' === $settings['type'] ) {


                echo '<div class="wrapper-options-list">';

                foreach ( $settings['options'] as $radio ) {

                    $radio_id = strtolower( str_replace( ' ', '-', $radio ) );

                    echo '<div class="single-option">';

                    printf( '<input type="radio" name="%1$s" id="%2$s" value="%3$s" /><label for="%2$s"><span><span></span></span>%3$s</label>',
                        $field,
                        $field_id . '_' . $radio_id,
                        $radio
                    );

                    echo '</div>'; // single-option

                }

                echo '</div>'; // wrapper-options-list


            } elseif ( 'checkbox' === $settings['type'] ) {


                echo '<div class="wrapper-options-list">';

                foreach ( $settings['options'] as $checkbox ) {

                    $checkbox_id = strtolower( str_replace( ' ', '-', $checkbox ) );

                    echo '<div class="single-option">';

                    printf( '<input type="checkbox" name="%1$s[]" id="%1$s_%2$s" value="%3$s" /><label for="%1$s_%2$s"><span><span></span></span>%3$s</label>',
                        $field,
                        $field_id . '-' . $checkbox_id,
                        $checkbox
                    );

                    echo '</div>'; // single-option

                }

                echo '</div>'; // wrapper-options-list


            } elseif ( 'select' === $settings['type'] ) {


                echo '<div class="wrapper-options-list">';
                echo '<select id="' . $field_id . ' name="' . $field . '">';

                // default to indicate they should select one
                echo '<option value="">(select one)</option>';

                foreach ( $settings['options'] as $option ) {

                    $option_id = strtolower( str_replace( ' ', '-', $option ) );

                    printf( '<option value="%s" />%s</option>',
                        $option_id,
                        $option
                    );

                }

                echo '</select>';
                echo '</div>'; // wrapper-options-list

            }

            echo '</div> <!-- .parent-field -->'; // parent-field
        }

        echo '<input type="submit" id="submitBtn" value="Submit" />';
        echo '</form>';
    }




    /*
     * Check that GnuPG is enabled and if we have a public key
     *
     */
    public function gpg_check( $gpgenv, $recipient ) {

        putenv( 'GNUPGHOME=' . $gpgenv );
        $gpg = new gnupg();
        $gpg->seterrormode( gnupg::ERROR_EXCEPTION ); // throw exception if error occurs

        try {
            $gpg->addencryptkey( $recipient );
        } catch ( Exception $e ) {
            die( 'ERROR: ' . $e->getMessage() );
        }

    }


    /*
     * Get the public key and send the email using PHPMailer
     */
    public function send_email( $rawmsg = array(), $smtp = array(), $gpgenv = '' ) {

        global $form_settings;

        // find the field that is the honeypot (only one field is supported for now)
        foreach ( $form_settings['fields'] as $field => $value ) {

            if ( is_array( $value ) && ! empty( $value['honeypot'] ) ) {
                $honeypot_field = $field;
            }
        }
        
        // determine if the honeypot field has values and return.
        $field_names = array_keys( $form_settings['fields'] );
        if ( ! empty( $honeypot_field ) && ! empty( $rawmsg[ $honeypot_field ] ) ) {
            echo 'Your message has been submitted.';
            return;
        }

        //die( var_dump($form_settings['fields'], $rawmsg) );

        // do GPG stuff first in case of errors
        putenv( 'GNUPGHOME=' . $gpgenv );
        $gpg = new gnupg();
        $gpg->seterrormode( gnupg::ERROR_EXCEPTION ); // throw exception if error occurs
        $gpg->addencryptkey( $smtp['recipient'] );

        $msg = ( isset( $smtp['noteToSelf'] ) ? $smtp['noteToSelf'] . "\n\n" : '' );
        foreach ( $rawmsg as $field => $input ) {

            if ( is_array( $input ) ) {
                $input = implode( ', ', $input );
            }

            $msg .= $field . ": " . strip_tags( $input ) . "\n"; 
        }

        unset( $rawmsg, $_POST ); // destroy user input
        $msg = $gpg->encrypt( $msg );

        try {
            $mail = new PHPMailer\PHPMailer\PHPMailer();

            $mail->isHTML( false );
            $mail->isSMTP();
            $mail->SMTPDebug = 0;
            $mail->SMTPAuth = true;
            $mail->CharSet = 'UTF-8';
            $mail->Host = $smtp['host'];
            $mail->Port = $smtp['port'];
            $mail->Username = $smtp['sender'];
            $mail->Password = $smtp['password'];
            $mail->setFrom( $smtp['sender'] );
            $mail->addAddress( $smtp['recipient'] );
            $mail->Recipient = $smtp['recipient'];
            $mail->Subject = ( isset( $smtp['subject'] ) ? $smtp['subject'] : 'Encrypted Email Form Submitted at ' . time() );
            $mail->Body = $msg;

            $mail->send();

            // seems like an exception is not thrown if there if valid error info from phpmailer
            if ( ! empty( $mail->ErrorInfo ) ) {
                echo '<strong>Something went wrong, please notify the owner of this site.</strong>';
                print_r( $mail->ErrorInfo );
            } else {
                echo '<h1 class="thankyou-title">Thank You</h1>';
                echo '<p class="thankyou-message">Your message has been securely sent via an encrypted message.</p>';
            }

            unset( $msg, $gpg );

        } catch ( Exception $e ) {

            echo '<strong>Message could not be send. PHPMailer error:</strong>';
            echo '<pre>';
            print_r( $mail->ErrorInfo );
            echo '</pre>';

        }
    }
}
