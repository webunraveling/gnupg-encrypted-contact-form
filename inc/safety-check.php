<?php

// force https
if ( $_SERVER['HTTPS'] != 'on' ) {
        header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
        exit();
}

// if optional security key is set check against it before loading anything
if ( defined( 'KEY' ) ) {

    if ( isset( $_GET ) && ! array_key_exists( 'key', $_GET ) || $_GET['key'] !== KEY ) {
        // var_dump( $_GET );
        die( '404 - page not found' );
    }

}


