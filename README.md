Note: The abbreviations PGP and GPG are often used interchangeably. Since GnuPG (GPG) is used in this implementation, I'll only refer to it as GPG.

This is a standalone contact form. Submitted content is encrypted with GnuPG and emailed over SMTP with PHPMailer. This is made to allow easy encryption without the end user knowing how to use GPG.

## Installation
At the moment, installation is not incredibly easy. This is still a work in progress. For installation, you'll need to be able to add a public GPG key to the server and edit a config file. These instructions assume you have SSH access. 

1. Clone this repo somewhere in your public html directory on a webserver. 
1. Import your public GPG key to the server.  
One common way would be to upload it to the server, login over SSH, and import it using `gpg -i myPublicKey.gpg`. (As shown in [this guide](https://www.debuntu.org/how-to-importexport-gpg-key-pair/))
1. Install PHPMailer using `composer`. Run the following command from the directory you have just cloned using `composer install`
1. Configure your settings by copying `config-sample.php` to `config.php` and editing it to your needs.
